## Kubernetes (k3s)

## Setup deployment server

- Server should have at least 4GB RAM and 2 CPUs. Lower if not installing Prometheus and Grafana.
- Configure VPS, see [VPS setup](../README.md#setup-deployment-server).
- Setup VPS Firewall. Allow only ports 22, 80, 443 and 6443.
- Install [k3s](https://docs.k3s.io/):

```shell
curl -sfL https://get.k3s.io | sh -s - \
	--write-kubeconfig ~/.kube/config \
	--write-kubeconfig-mode 600 \
	--kubelet-arg "--image-gc-high-threshold=10" \
	--kubelet-arg "--image-gc-low-threshold=5"
```

- Install [Helm](https://helm.sh/docs/intro/install/) on your local machine:

```shell
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

### Cluster example

```shell
kubectl create deployment nginx --image=nginx --port=80 --replicas=2
kubectl get pods

kubectl create service clusterip nginx --tcp=80:80
kubectl describe service nginx

echo "apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx
  annotations:
    ingress.kubernetes.io/ssl-redirect: \"false\"
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx
            port:
              number: 80" > nginx-ingress.yaml

kubectl apply -f nginx-ingress.yaml
kubectl describe ingress nginx
```

Now, the nginx is accessible on http://{IP}/.

To delete the deployment, run `kubectl delete deployment nginx`

## Setup DNS

Add DNS records for the cluster (e.g. `dog.example.com` and `*.dog.example.com`).

## Connect to the cluster from local machine

On the local machine, install [kubectl](https://kubernetes.io/docs/tasks/tools/) and
[helm](https://helm.sh/docs/intro/install/).

On the remote host, open file `/etc/rancher/k3s/k3s.yaml` and copy its contents to the local machine's
file `~/.kube/config`. Replace `server` value with real IP (e.g. `https://5.5.5.5:6443`).

If you already have some contexts/clusters there, you need to add records to each section
(clusters, contexts, users). In this case, ensure correct naming (instead of `default`).

Now, validate data and switching context by running:

```shell
kubectl config get-contexts
kubectl config use-context <context>
```

Additionally, to `kubectl`, there is also a "cli-gui" called `k9s`, see https://k9scli.io/ for more details.

## Management Tools

Define some environment variables use in the following commands:

```shell
export DOMAIN=dog.example.com
export EMAIL=name@example.com
```

In every step, ensure your working directory is `cd ~/deploy/k3s` (where you have cloned this repository to).

### Traefik

Traefic is shipped with k3s, but we need to change some configuration:

```shell
cat traefik.yaml | envsubst | kubectl apply -f -
```

### Cert-Manager and Let's Encrypt

```shell
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.5/cert-manager.yaml
```

Wait a few seconds when all pods are running and then continue with next steps.

```shell
cat letsencrypt.yaml | envsubst | kubectl apply -f -
```

### Redirecting to WWW subdomain

Install predefined middlewares:

```shell
cat traefik-middlewares.yaml | envsubst | kubectl apply -f -
```

Add the following annotations to the ingress:

```yaml
    spec.ingressClassName: traefik
    traefik.ingress.kubernetes.io/router.middlewares: default-redirect-to-www@kubernetescrd
```

Separate multiple middlewares with comma.

### Basic Auth

This requires `htpasswd` and `openssl` to be installed on the local machine.

Install them with the following command:

```shell
sudo apt update && sudo apt install apache2-utils
```

```shell
    export USER=admin
    export PASSWORD=secret
    export BASIC_AUTH=$(htpasswd -nb $USER $PASSWORD | openssl base64)
```

```shell
cat traefik-basic-auth.yaml | envsubst | kubectl apply -f -
```

To add Basic Auth to any ingress, add the following annotations:

```yaml
    spec.ingressClassName: traefik
    traefik.ingress.kubernetes.io/router.middlewares: default-my-basic-auth@kubernetescrd
```

These annotations are used in some of the following manifests to secure endpoints.

### Monitoring

We install all metrics-related tools to `monitoring` namespace.

Create namespace:

```shell
kubectl create ns monitoring
```

Update prometheus-community helm repository and install:

```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts && \
helm repo update
helm upgrade --install monitoring-stack prometheus-community/kube-prometheus-stack -n monitoring -f monitoring-values.yaml
```

Deploy metrics ingresses:

```shell
cat monitoring-ingress.yaml | envsubst | kubectl apply -f -
```

Now, visit https://alert-manager.dog.example.com, https://prometheus.dog.example.com and https://grafana.dog.example.com.

#### Traefik metrics

```shell
cat traefik-metrics.yaml | envsubst | kubectl apply -f -
kubectl apply -f traefik-dashboard.yaml
```

#### Logs

For logs, we use [loki](https://grafana.com/oss/loki/).

```shell
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm upgrade --install loki grafana/loki-stack -n monitoring -f loki-values.yaml
```

Loki Datasource should be automatically added after Grafana restart. To restart, use following:

```shell
kubectl get pods -n monitoring
kubectl delete pod <grafana-pod-name> -n monitoring
```

## Pulling from private container registry

Add a new secret via Helm (preferred):

```yaml
# values.yaml
dockerRegistry:
  server: registry.gitlab.com
  username: ${DOCKER_REGISTRY_USERNAME}
  password: ${DOCKER_REGISTRY_PASSWORD}

```

```yaml
# templates/secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlab-registry
  namespace: {{ .Values.app.namespace }}
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: {{ printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"auth\":\"%s\"}}}" .Values.dockerRegistry.server .Values.dockerRegistry.username .Values.dockerRegistry.password (printf "%s:%s" .Values.dockerRegistry.username .Values.dockerRegistry.password | b64enc) | b64enc }}

```

Add a new secret (manually, if not using Helm):

```shell
kubectl create secret docker-registry gitlab-registry \
  --docker-server=registry.gitlab.com \
  --docker-username=<your-username> \
  --docker-password=<your-access-token> # read_registry privileges
```

Then, use these secrets in your chart, for example:

```yaml
# your-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: your-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: your-app
  template:
    metadata:
      labels:
        app: your-app
    spec:
      containers:
        - name: your-app-container
          image: registry.gitlab.com/your-group/your-repo:your-tag
      imagePullSecrets:
        - name: gitlab-registry

```

When deploying this manifest into the cluster (`kubectl apply -f your-deployment.yaml`),
image should be correctly pulled and started).

## Gitlab CI deployment

For detailed and up-to-date documentation, see https://docs.gitlab.com/ee/user/clusters/agent/install/index.html

### Create a new agent

https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#create-an-agent-configuration-file

Create a new Gitlab repository under a group with your projects (e.g. `path/to/agent/project/cicd-agent`),
put there an empty file with agent name in its path.

Agent name has to be unique in the project. May be named as the cluster/VPS etc.

To be able to access other projects or whole (sub)groups, add a content to the file.

Here is an example:

```yaml
# .gitlab/agents/<agent-name>/config.yaml
ci_access:
  projects:
    - id: bar/baz
  groups:
    - id: foo
```

It may be useful **agent-name** to use agent's environment in its name.
For example, `agent-stage` and `agent-prod`.

*Note: Agent can only access projects and groups within the agent repository hierarchy.
So only in the same groups or subgroups.*

### Register the agent with GitLab

Navigate to `path/to/agent/project/cicd-agent` project.

Go to **Operate > Kubernetes clusters** on the left panel of the Gitlab project.
Select **Connect a cluster (agent)** and select agent with the name from the previous step
(agent directory name). Click **Register**. Gitlab shows you a secret token and instructions
how to install the agent into the Kubernetes cluster. Follow the instructions (helm is required).

Just be sure you are working with the correct context:

```shell
kubectl config current-context
kubectl config use-context <your-context>
```

If you need to just upgrade the agent, use the following command:

```shell
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
  --namespace gitlab-agent
```

Now, in the project you want to deploy, add `.gitlab-ci.yml` with following contents:

```yaml
# .gitlab-ci.yml
deploy:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config get-contexts
    - kubectl config use-context path/to/agent/project:agent-name
    - kubectl get pods
    - helm upgrade --install project-name ./resources/deploy --values ./resources/deploy/values.yaml
    - kubectl rollout status deployment/project-name
```

*TIP: Instead of this, you can use a predefined CI templates from this repository.*

## Accessing MySQL in a Pod

```shell
kubectl get pods -l app=carspneu-mysql
kubectl port-forward <pod-name> 3306:3306
```
Afterwards, open e.g. DataGrip, connect to the database (use localhost:3306) and run needed SQL files. If want to load a mysql dump file, just copy it into the project, open and Run.

To perform mysqldump or restoring with mysql, it is necessary to install mysql-client on the local machine:

```shell
sudo apt-get update
sudo apt-get install mysql-client
```

## Links

- https://k3s.rocks/
- https://docs.gitlab.com/ee/user/clusters/agent/install/index.html
