## Docker Swarm

## Setup deployment server

- Install Docker (on Ubuntu or use pre-configured Docker VPS)
- Setup VPS Firewall. Allow only ports 22, 80 and 443.
- Initialize Docker Swarm:

    ```
    docker swarm init
    ```

### Traefik

Traefik is "edge router". It manages all the traffic and makes routing in the services usable.

```
# setup network
docker network create --driver=overlay traefik-public
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add traefik-public.traefik-public-certificates=true $NODE_ID

# set env variables
export DOMAIN=traefik.example.com
export EMAIL=admin@example.com
export ADMIN_USER=admin
export ADMIN_PASSWORD=changethis
export HASHED_PASSWORD=$(openssl passwd -apr1 $ADMIN_PASSWORD)

# deploy traefik from the repository
cd ~/deploy/docker-swarm/traefik
docker stack deploy -c traefik.yml traefik
```

You can check the dashboard at the configured domain.

#### Links

- https://dockerswarm.rocks/
- https://doc.traefik.io/traefik/
- https://www.benjaminrancourt.ca/how-to-redirect-from-non-www-to-www-with-traefik/

### Loki

Grafana loki is like Prometheus, but for logs. It is lightweight alternative to ELK stack.

```
# install loki-docker-driver to pass logs from containers to loki
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions

# log from all container into loki by default
echo '{
    "debug" : false,
    "log-driver": "loki",
    "log-opts": {
        "loki-url": "http://127.0.0.1:3100/loki/api/v1/push"
    }
}' | sudo tee /etc/docker/daemon.json

# restart docker
sudo systemctl restart docker

# set env variables
export DOMAIN=grafana.example.com
export ADMIN_USER=admin
export ADMIN_PASSWORD=admin

# deploy loki from the repository
cd ~/deploy/docker-swarm/loki
docker stack deploy -c loki.yml loki
```

Go into the Grafana web interface via set domain,
Connections > Data Sources and add Loki Data Source.

The only required field is URL: http://loki:3100

#### Security

Ensure that port 3100 is not publicly accessible. It is good practise to setup VPS firewall.

Alternatively reject all traffic to port 3100:

```
iptables -I DOCKER-USER -i eth0 -p tcp --destination-port 3100 -j REJECT
```

#### Dashboards

To see the logs in Grafana, it is needed to create a Dashboard.

You can import a very simple predefined Dashboard.

In Grafana, go to Dashboards > New > Import and paste a JSON:

- [Log Overview Dashboard](docker-swarm/loki/dashboards/log-overview.json)

![image](docker-swarm/loki/dashboards/log-overview.png)

#### Links

- https://grafana.com/oss/loki/
- https://medium.com/@mrschneider/grafana-loki-in-docker-swarm-78bfa6a761fa
- https://github.com/swarmstack/loki
- https://drailing.net/2020/06/running-loki-and-grafana-on-docker-swarm/

### Portainer

Portainer allows you to manage Docker Swarm deployments, create webhooks which could be called
from a deployment pipeline etc.

Portainer CE is free to use. There is also Portainer BE which allows to manage webhooks
and has more features. It is [free up to 3 nodes](https://www.portainer.io/take-3).

```
# set env variables
export DOMAIN=portainer.example.com
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add portainer.portainer-data=true $NODE_ID

# deploy portainer from the repository
cd ~/deploy/docker-swarm/portainer
docker stack deploy -c portainer.yml portainer
```

Now login to the Portainer via set domain and set credentials.

Make sure you do that in a timely manner otherwise Portainer locks itself due to security reasons.

#### Adding Docker Registry

To allow Portainer pull images from your registry, you need to configure it first.

Go to the Settings > Registries and Add Registry.

Create a project Token or misuse "service user". Token needs Read Api permissions.

#### Adding a Stack

Select desired environment, click Stacks and Add stack.

Prefer to use **Repository** since the configuration can be easily managed, changed and versioned.
It also supports webhooks to trigger application deployment which can be used in a deployment pipeline.

Probably you will need to set up Git Authentication to access your repository.
Create a project Token or misuse "service user". Token needs Read Repository permissions.

Type repository URL, reference and path to a compose yml file (it might be nested in your project structure).

You can enable **Automatic Updates** and set up a webhook. That webhook can be triggered from a deployment pipeline.

In the case you want to trigger deployments manually, you could use Web Editor and do manual updates and deployments.

#### Links

- https://dockerswarm.rocks/
- https://www.portainer.io/
