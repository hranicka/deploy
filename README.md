# Application Deployment

[[_TOC_]]

## Setup project CI

Add `.gitlab-ci.yml` into the root directory of your project with following content:

TODO: Add `resources/docker`, its structure and example files.

### Symfony Application

```
include:
  - project: 'hranicka/deploy'
    ref: 'master'
    file:
      - 'gitlab-ci/workflow.yml'
      - 'gitlab-ci/symfony-application-rr.yml'

variables:
  PHP_CI_VERSION: "8.4-cli-alpine-dev"
  PHP_VERSION: "8.4-fpm-alpine"
  NODE_VERSION: "22-alpine"
  KUBE_PROJECT: hello-world
  KUBE_NAMESPACE: default

deploy:stage:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-stage

deploy:prod:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-prod

```

### Go Application

```
include:
  - project: 'hranicka/deploy'
    ref: 'master'
    file:
      - 'gitlab-ci/workflow.yml'
      - 'gitlab-ci/go-application.yml'

variables:
  GO_VERSION: "1.24-alpine"
  KUBE_PROJECT: hello-world
  KUBE_NAMESPACE: default

deploy:stage:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-stage

deploy:prod:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-prod

```

### Node.js Application

```
include:
  - project: 'hranicka/deploy'
    ref: 'master'
    file:
      - 'gitlab-ci/workflow.yml'
      - 'gitlab-ci/node-application.yml'

variables:
  NODE_VERSION: "22-alpine"
  KUBE_PROJECT: hello-world
  KUBE_NAMESPACE: default

deploy:stage:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-stage

deploy:prod:
  variables:
    KUBE_AGENT: /path/to/project:agent-name-prod

```

## Setup deployment server

- Set up a clean Ubuntu server.
- Add your SSH key.
- Setup the server (you can use "cloud init script" or similar functionality provided by your VPS provider):

```shell
#!/usr/bin/env bash
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config
sudo service ssh restart
sudo timedatectl set-timezone Europe/Prague
git clone https://gitlab.com/hranicka/deploy.git ~/deploy
```

### Kubernetes cluster (k3s)

Follow the [k3s](k3s/README.md) guide.

### Docker Swarm

Follow the [Docker Swarm](docker-swarm/README.md) guide.

_I would recommend to use Kubernetes through k3s since its support and documentation
is currently much better than Docker Swarm's. It also offers scheduled jobs and more
robust networking options than Swarm._
